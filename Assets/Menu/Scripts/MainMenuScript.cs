using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
    [SerializeField] private ScoreScriptableObject score;
    [Header("Audio")]
    [SerializeField] private AudioMixer mixer;
    
    [Header("Animator")]
    [SerializeField] private Animator creditsAnimator;
    [SerializeField] private Animator modeAnimator;

    public void Play(int players)
    {
        PlayerPrefs.SetInt("MaxPlayers", players);
        score.NewGame = true;
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void UpdateSlider(float value)
    {
        mixer.SetFloat("Volume", Mathf.Log10(value) * 20);
    }

    public void SetCreditsAnimator(bool value)
    {
        creditsAnimator.SetBool("IsIn", value);
    }

    public void SetModeAnimator(bool value)
    {
        modeAnimator.SetBool("IsIn", value);
    }

    public void ButtonHover()
    {
        SoundManager.instance.PlaySound("Hover");
    }

    public void ButtonPressed()
    {
        SoundManager.instance.PlaySound("Click");
    }
}
