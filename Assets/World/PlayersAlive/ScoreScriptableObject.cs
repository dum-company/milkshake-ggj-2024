using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Score", menuName = "ScriptableObjects/Score", order = 1)]
public class ScoreScriptableObject : ScriptableObject
{
    public int PlayerOneScore = 0;
    public int PlayerTwoScore = 0;
    public int PlayerThreeScore = 0;
    public int PlayerFourScore = 0;
    public bool NewGame = true;

}
