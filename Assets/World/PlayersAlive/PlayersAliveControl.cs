using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayersAliveControl : MonoBehaviour
{
    [SerializeField] GameObject victoryCanvas;
    [SerializeField] TextMeshProUGUI victoryText;
    [SerializeField] ScoreScriptableObject score;
    [SerializeField] int roundLimit = 5;
    public static List<string> players;


    void Start()
    {
        if(score.NewGame){
            score.PlayerOneScore = 0;
            score.PlayerTwoScore = 0;
            score.PlayerThreeScore = 0;
            score.PlayerFourScore = 0;

            score.NewGame = false;
        }

        players = new List<string>();
    }

    void Update()
    {
        if (players.Count == 1 && Time.timeScale != 0f)
        {
            Time.timeScale = 0f;
            victoryCanvas.SetActive(true);
            victoryText.text = players[0] + " wins this round!!";
            addScore(players[0]);
            determineWinner();

        }
    }

    void addScore(string player){
        if(players[0] == "Player1") score.PlayerOneScore++;
        if(players[0] == "Player2") score.PlayerTwoScore++;
        if(players[0] == "Player3") score.PlayerThreeScore++;
        if(players[0] == "Player4") score.PlayerFourScore++;
    }

    void determineWinner(){
        if(score.PlayerOneScore == roundLimit){
            victoryText.text = "\nPlayer1 wins the game!!";
            score.NewGame = true;

        }
        if(score.PlayerTwoScore == roundLimit){
            victoryText.text += "\nPlayer2 wins the game!!";
            score.NewGame = true;

        }
        if(score.PlayerThreeScore == roundLimit){
            victoryText.text += "\nPlayer3 wins the game!!";
            score.NewGame = true;

        }
        if(score.PlayerFourScore == roundLimit) {
            victoryText.text += "\nPlayer4 wins the game!!";
            score.NewGame = true;
        }

    }

    public void RestartScene(){
        int scene = UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex;
        UnityEngine.SceneManagement.SceneManager.LoadScene(scene, UnityEngine.SceneManagement.LoadSceneMode.Single);
    }
}
