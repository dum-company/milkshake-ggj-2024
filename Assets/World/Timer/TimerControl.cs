using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TimerControl : MonoBehaviour
{
    [SerializeField] int TimeLimit;
    [SerializeField] TextMeshProUGUI timerText;
    float timer;
    public static bool timeOut;
     void Start(){
        timer = 0f;
        timeOut = false;
    }

    void Update(){
        if(!timeOut)
            countDown();
        else 
        timerText.text = "Burger time!!!";

    }

    private void countDown()
    {
        timer += Time.deltaTime;
        if (timer >= 1f){
            TimeLimit -= 1;
            timer = 0f;
        }
         
        timerText.text = showNiceTime(TimeLimit);

        if (TimeLimit == 0){
            timeOut = true;
        }
    }

    string showNiceTime(int timeLimit)
    {
        int minutes = Mathf.FloorToInt(timeLimit / 60F);
        int seconds = Mathf.FloorToInt(timeLimit - minutes * 60);
        return string.Format("{0:0}:{1:00}", minutes, seconds);
    }
}
