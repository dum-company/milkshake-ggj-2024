using UnityEngine;

public class CodeTexture : MonoBehaviour {
    [SerializeField] private SkinnedMeshRenderer cowMesh;
    [SerializeField] private SkinnedMeshRenderer jacketMesh;
    [SerializeField] private Material cowMaterial;
    [SerializeField] private Material jacketMaterial;
    [SerializeField] private Sprite[] cowTextures;
    [SerializeField] private Sprite[] jacketTextures;
    private int id;

    public void ChangeTexture(int id)
    {
        this.id = id;

        Material newCowMaterial = new Material(cowMaterial);
        Material newJacketMaterial = new Material(jacketMaterial);

        newCowMaterial.mainTexture = cowTextures[id].texture;
        newJacketMaterial.mainTexture = jacketTextures[id].texture;

        cowMesh.material = newCowMaterial;
        jacketMesh.material = newJacketMaterial;
    }
    
}
