using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [Header("Colors")]
    [SerializeField] private CowColorPalette[] cowColors;

    [Header("Bar")]
    [SerializeField] private RectTransform self;
    [SerializeField] private Image weaponBackground;
    [SerializeField] private Image background;
    [SerializeField] private Image bar;
    [SerializeField] private Image dots;
    [SerializeField] private Image circle;
    [SerializeField] private Image weapon;
    [SerializeField] private Sprite[] weapons;

    public void SetPositions(RectTransform barTransform)
    {
            self.anchorMin = barTransform.anchorMin;
            self.anchorMax = barTransform.anchorMax;
            self.anchoredPosition = barTransform.anchoredPosition;
    }

    public void SetID(int id)
    {
        weaponBackground.color = cowColors[id].primaryColor;
        background.color = cowColors[id].primaryColor;
        bar.color = cowColors[id].primaryColor;
        circle.color = cowColors[id].primaryColor;
        dots.color = cowColors[id].secondaryColor;
        weapon.color = cowColors[id].secondaryColor;
    }

    public void SetHealth(float value)
    {
        bar.fillAmount = value;
        dots.fillAmount = value;
    }

    public void SetWeapon(int i)
    {
        weapon.sprite = weapons[i];
    }
}