using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Cinemachine;
using TMPro;

public class PlayerSpawn : MonoBehaviour
{

    [Header("Camera")]
    [SerializeField] CinemachineTargetGroup targetGroup;

    [Header("Player Spawn")]
    [SerializeField] Transform[] playerSpawnPoints;
    [SerializeField] PlayerInputManager playerInputManager;
    
    [Header("SpawnScreen")]
    [SerializeField] TextMeshProUGUI pressButtonText;
    [SerializeField] GameObject spawnScreenCanvas;


    [Header("Player Count")]
    public int playerCount = 0;
    int maxPlayers;

    Dictionary<int,GameObject> players = new Dictionary<int, GameObject>();

    void Start()
    {
        maxPlayers = PlayerPrefs.GetInt("MaxPlayers", 2);
        initializeReadyScreen();
    }

    public void JoinPlayer(PlayerInput player){
        setPlayerReady(player.gameObject);
        updatePlayerCountText();
        if(playerCount == maxPlayers) readyAllPlayersAndStart();

        Debug.Log("Player " + playerCount + " joined");   
    }



    void setPlayerReady(GameObject player){
        playerCount++;
        players.Add(playerCount,player);
        setPlayerGameObject(player);
        addPlayerToCameraTargetGroup(player.transform);
        player.layer = LayerMask.NameToLayer("Player" + playerCount);
        player.GetComponent<PlayerHealth>().SetPlayerID(playerCount - 1);

        player.GetComponent<CodeTexture>().ChangeTexture(playerCount - 1);

        PlayersAliveControl.players.Add(player.name);

        if (playerCount >= maxPlayers)
            playerInputManager.DisableJoining();
    }

    void setPlayerGameObject(GameObject player){
        player.name = "Player" + playerCount;
        player.transform.position = playerSpawnPoints[playerCount - 1].position;
        player.GetComponent<PlayerInput>().SwitchCurrentActionMap("Null");
    }

    void readyAllPlayersAndStart(){
        spawnScreenCanvas.SetActive(false);
        Time.timeScale = 1f;
        foreach(KeyValuePair<int, GameObject> player in players)
            player.Value.GetComponent<PlayerInput>().SwitchCurrentActionMap("Player");
    }
    void addPlayerToCameraTargetGroup(Transform player){
        CinemachineTargetGroup.Target[] allTargets = new CinemachineTargetGroup.Target[targetGroup.m_Targets.Length + 2];
        
        targetGroup.m_Targets.CopyTo(allTargets, 0);
        allTargets[allTargets.Length - 1] = new CinemachineTargetGroup.Target
            {
                target = player,
                weight = 1f,
                radius = 1f
            };
        targetGroup.m_Targets = allTargets;
    }

    void updatePlayerCountText(){
        pressButtonText.text = "";
        if(playerCount < maxPlayers){
            for(int i = 0; i < maxPlayers; i++){
                if(i < playerCount)
                    pressButtonText.text += "Player " + (i + 1) + ": READY\n";
                else
                    pressButtonText.text += "Player " + (i + 1) + ": Not ready\n";
            }
            pressButtonText.text += "\n\n\nPress any button on Player " + (playerCount + 1) + " to join\n";
        }        
    }


    void initializeReadyScreen(){
        // pause the game 
        Time.timeScale = 0f;
        spawnScreenCanvas.SetActive(true);
        pressButtonText.text = "";
        for(int i = 0; i < maxPlayers; i++){
            pressButtonText.text += "Player " + (i + 1) + ": Not ready\n";
        }
        pressButtonText.text += "\n\n\nPress any button on Player " + (playerCount + 1) + " to join\n";

    }
}
