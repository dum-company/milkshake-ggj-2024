using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerAttack : MonoBehaviour
{
    [SerializeField] private Animator anim;
    [SerializeField] private PlayerHealth pH;
    [SerializeField] private SoundPlayer sP;
    [SerializeField] private IPlayerWeapon[] weapons;
    private int selectedWeapon = 0;
    private bool isUsing = false;

    public bool GetUsing()
    {
        return isUsing;
    }

    public void SetUsing(bool value)
    {
        isUsing = value;
        anim.SetBool("attacking", isUsing);
    }

    public void SelectWeapon(int weapon, bool isBeingUsed)
    {
        selectedWeapon = weapon;
        SetUsing(isBeingUsed);
        anim.SetInteger("weaponSelected", weapon);
        weapons[selectedWeapon].SetMaxAmmo();
        pH.SelectWeapon(weapon);
        if (weapon == 0)
            StartCoroutine(returnToShooting());
        else
            useWeapon();
    }

    private IEnumerator returnToShooting()
    {
        anim.SetBool("attacking", false);
        yield return new WaitForSeconds(0.5f);
        useWeapon();
    }

    void Awake()
    {
        weapons = GetComponentsInChildren<IPlayerWeapon>();
    }

    public void OnUse(InputAction.CallbackContext context)
    {
        SetUsing(context.performed);
        useWeapon();
    }

    private void useWeapon()
    {
        if (isUsing)
            anim.SetBool("attacking", true);
        weapons[selectedWeapon].PlayerScript = this;
        weapons[selectedWeapon].UseWeapon(isUsing);
    }
}
