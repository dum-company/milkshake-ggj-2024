using UnityEngine;

public class PowerUp : MonoBehaviour
{
    const int POWER_UP_AMOUNT = 6;
    const int WEAPON_POWERUPS = 3;
    [SerializeField] private SoundPlayer sP;
    private PowerUpSpawner spawner;

    public void SetSpawner(PowerUpSpawner spawn)
    {
        spawner = spawn;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            int random = Random.Range(0, POWER_UP_AMOUNT);
            if (random < WEAPON_POWERUPS)
            {
                PlayerAttack pA = col.gameObject.GetComponent<PlayerAttack>();
                pA.SelectWeapon(random + 1, pA.GetUsing());
                sP.PlaySound("PickUp");
            } else
            {
                Debug.Log(random - 3);
                spawner.CallEvents(random-3);
            }
            Destroy(this.gameObject, 0.2f);
        }
    }

    void OnDestroy()
    {
        spawner.RemovePowerUp();
    }
}
