using Unity.VisualScripting.Dependencies.Sqlite;
using UnityEngine;
using UnityEngine.InputSystem;
using Utils;

public class PlayerMovementScript : MonoBehaviour
{
    [SerializeField] private Rigidbody rb;
    [SerializeField] private Animator anim;
    [SerializeField] private SoundPlayer sP;

    [Header("Movement")]
    [SerializeField, Range(0, 3)] private float drag;
    [SerializeField] private float moveForceNormal;
    [SerializeField] private float moveForceDulceOfMilk;
    [SerializeField, Range(0, 20)] private float maxVelocity;
    
    private Vector3 lookPosition = Vector3.zero;
    private bool moving = false;
    private float currentMoveForce;
    private string currentStepSound = "Step";


    [Header("Jump")]
    [SerializeField] private float jumpVelocity;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private LayerMask groundLayer;
    [SerializeField, Range(0, 1)] private float groundCheckLenght;
    bool grounded = false;

    [Header("Charge")]
    [SerializeField, Range(0, 20)] private int chargeDamage;
    [SerializeField] private float chargeVelocity;
    [HideInInspector] public bool charging = false;

    private Transform barbecueTransform;

    
    void Start()
    {
        barbecueTransform = GameObject.Find("BarbecueSpawnPoint").transform;
        rb.drag = drag;
        currentMoveForce = moveForceNormal;
    }

    void Update()
    {
        checkGround();
    }

    private void checkGround()
    {
        bool prev = grounded;
        grounded = Physics.Raycast(groundCheck.position, - Vector3.up, groundCheckLenght + 0.1f, groundLayer);
        if (grounded && !prev)
            sP.PlaySound("Fall");
    }

    void FixedUpdate()
    {
        move();
    }

    private void move()
    {
        if (charging)
        {
            rb.velocity = transform.forward * chargeVelocity;
            return;
        }
        if (moving)
        {
            sP.PlaySound(currentStepSound);
            rb.AddForce(transform.forward * currentMoveForce);
        }

        if (TimerControl.timeOut){
            goToBarbecue();
        }
            
        clampVelocity();
    }

    void goToBarbecue(){
        Vector3 attractionDir = transform.position - barbecueTransform.position;
        rb.AddForce(-attractionDir.normalized * (currentMoveForce /1.5f), ForceMode.Force);
    }

    private void clampVelocity()
    {
        Vector3 vel = new Vector3(rb.velocity.x, 0, rb.velocity.z);
        if (vel.magnitude > maxVelocity)
        {
            vel = Vector3.ClampMagnitude(vel, maxVelocity);
            rb.velocity = new Vector3(vel.x, rb.velocity.y, vel.z);
        }
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        Vector2 value = context.ReadValue<Vector2>();
        if (value == Vector2.zero)
        {
            sP.StopSound("SweetStep");
            sP.StopSound("Step");
        }
        if (charging)
            return;
        lookPosition.x = transform.position.x + value.x;
        lookPosition.y = transform.position.y;
        lookPosition.z = transform.position.z + value.y;
        transform.LookAt(lookPosition, Vector3.up);
        moving = value.x != 0 || value.y != 0;
        anim.SetBool("walking", moving);
        anim.SetBool("idle", !moving);
    }

    public void OnJump(InputAction.CallbackContext context)
    {
        if (grounded)
            rb.velocity = new Vector3(rb.velocity.x, jumpVelocity, rb.velocity.z);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.DrawRay(groundCheck.position, - groundCheck.up * groundCheckLenght);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "DulceOfMilk")
        {
            currentMoveForce = moveForceDulceOfMilk;
            currentStepSound = "SweetStep";
            sP.StopSound("Step");
        }
        if (charging && collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<PlayerHealth>().Damage(chargeDamage);
            sP.PlaySound("ChargeImpact");
        }
        if (collision.gameObject.tag == "Barbecue")
            rb.velocity = new Vector3(rb.velocity.x, jumpVelocity*3, rb.velocity.z);
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "DulceOfMilk")
        {
            currentMoveForce = moveForceNormal;
            currentStepSound = "Step";
            sP.StopSound("SweetStep");
        }
    }
}
