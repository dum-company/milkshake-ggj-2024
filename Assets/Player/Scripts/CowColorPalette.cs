using UnityEngine;

[System.Serializable]
public class CowColorPalette
{
    public Color primaryColor;
    public Color secondaryColor;
}
