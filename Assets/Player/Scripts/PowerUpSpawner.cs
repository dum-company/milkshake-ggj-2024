using System;
using UnityEngine;

public class PowerUpSpawner : MonoBehaviour
{
    public static event Action<int> TrapEvent;

    [SerializeField] private GameObject powerUpPrefab;
    [SerializeField, Range(0, 5)] private float cooldown = 3;
    float timer;
    private Transform[] positions;
    int powerUpAmount = 0;
    
    void Start()
    {
        positions = transform.GetComponentsInChildren<Transform>();
        timer = cooldown;
    }

    void Update()
    {
        if (powerUpAmount < positions.Length - 1)
        {
            if (timer <= 0)
            {
                spawnPowerUp();
                timer = cooldown;
            } else
                timer -= Time.deltaTime;
        }
    }

    private void spawnPowerUp()
    {
        int i = getPosition();
        instantiatePowerUp(i);
    }

    private int getPosition()
    {
        bool chosen = false;
        int i;
        do
        {
            i = UnityEngine.Random.Range(0, positions.Length);
            chosen = (positions[i].childCount == 0);
        } while (!chosen);
        return i;
    }

    private void instantiatePowerUp(int i)
    {
        powerUpAmount++;
        GameObject go = GameObject.Instantiate(powerUpPrefab);
        go.transform.position = positions[i].position;
        go.transform.parent = positions[i];
        go.GetComponent<PowerUp>().SetSpawner(this);
    }

    public void RemovePowerUp()
    {
        powerUpAmount--;
    }

    public void CallEvents(int id)
    {
        TrapEvent?.Invoke(id);
    }
}
