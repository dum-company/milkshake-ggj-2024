using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    [HideInInspector] public int playerID = 0;
    [SerializeField] private SoundPlayer sP;

    [Header("Bar")]
    [SerializeField] private RectTransform[] barPositions;
    [SerializeField] private GameObject barPrefab;
    private HealthBar hB;

    [Header("Stats")]
    [SerializeField] private int maxHealth;
    private int currentHealth;

    void Start()
    {
        currentHealth = maxHealth;
    }

    public void SetPlayerID(int value)
    {
        playerID = value;
        spawnBar();
    }

    private void spawnBar()
    {
        RectTransform elementTransform = barPositions[playerID].GetChild(0).GetComponent<RectTransform>();
        GameObject bar = GameObject.Instantiate(barPrefab, barPositions[playerID]);
        hB = bar.GetComponent<HealthBar>();
        hB.SetPositions(elementTransform);
        hB.SetID(playerID);
    }

    public void Damage(int amount)
    {
        currentHealth -= amount;
        sP.PlaySound("Hit");
        sP.PlaySound("Hurt");
        hB.SetHealth(((float)currentHealth)/((float)maxHealth));
        if (currentHealth <= 0)
        {
            foreach (string p in PlayersAliveControl.players)
            {
                if (p == gameObject.name)
                {
                    PlayersAliveControl.players.Remove(p);
                    break;
                }
            }
            gameObject.SetActive(false);
        }
    }

    public void SelectWeapon(int i)
    {
        hB.SetWeapon(i);
    }
}
