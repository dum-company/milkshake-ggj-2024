using UnityEngine;

public class MilkBullet : MonoBehaviour
{
    [SerializeField] private Rigidbody rb;
    [SerializeField] private float speed;
    [SerializeField] private float duration;
    [SerializeField, Range(0, 20)] private int damage;
    bool move = false;

    public void SetTarget(Vector3 value)
    {
        Vector3 target = transform.position;
        target.x = transform.position.x + value.x;
        target.z = transform.position.z + value.z;
        transform.LookAt(target, Vector3.up);
        move = true;
        rb.velocity = transform.forward * speed;
    }

    void Update()
    {
        duration -= Time.deltaTime;
        if (duration <= 0)
            Destroy(this.gameObject);
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.transform.tag == "Player")
            col.gameObject.GetComponent<PlayerHealth>().Damage(damage);
        Destroy(this.gameObject);
    }
}
