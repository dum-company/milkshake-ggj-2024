using UnityEngine;

public class UdderWeapon : MonoBehaviour, IPlayerWeapon
{
    #region interface
    public GameObject Object 
    {
        get => this.gameObject;
    }

    private PlayerAttack playerScript;
    public PlayerAttack PlayerScript 
    {
        get => playerScript;
        set => playerScript = value;
    }

    public void UseWeapon(bool performed)
    {
        usingWeapon = performed;
    }

    public void NoAmmo()
    {
        return;
    }

    public void SetMaxAmmo()
    {
        return;
    }
    #endregion

    [SerializeField] private SoundPlayer sP;
    [SerializeField] private GameObject bullet;
    [SerializeField, Range(0, 2)] private float cooldown;
    float timer = 0;
    private bool usingWeapon = false;

    void Update()
    {
        if (timer > 0)
            timer -= Time.deltaTime;
        if (usingWeapon && timer <= 0)
        {
            timer = cooldown;
            shoot();
        }
    }

    private void shoot()
    {
        sP.PlaySound("Shoot");
        GameObject spawnedBullet = GameObject.Instantiate(bullet, transform.position, Quaternion.identity);
        spawnedBullet.layer = transform.parent.gameObject.layer;
        spawnedBullet.GetComponent<MilkBullet>().SetTarget(transform.forward);
    }
}
