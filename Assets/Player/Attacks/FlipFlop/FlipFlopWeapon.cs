using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

public class FlipFlopWeapon : MonoBehaviour, IPlayerWeapon
{
    #region interface
    public GameObject Object 
    {
        get => this.gameObject;
    }

    private PlayerAttack playerScript;
    public PlayerAttack PlayerScript 
    {
        get => playerScript;
        set => playerScript = value;
    }

    public void UseWeapon(bool performed)
    {
        usingWeapon = performed;
    }

    public void NoAmmo()
    {
        PlayerScript.SelectWeapon(0, usingWeapon);
        usingWeapon = false;
        timer = 0;
        SetMaxAmmo();
    }

    public void SetMaxAmmo()
    {
        ammo = maxAmmo;
    }
    #endregion

    [SerializeField] private GameObject bullet;
    [SerializeField, Range(1, 5)] private int maxAmmo = 1;
    [SerializeField, Range(0, 5)] private float cooldown = 1f;
    private int ammo;
    float timer = 0;
    private bool usingWeapon = false;

    void Update()
    {
        if (timer > 0)
            timer -= Time.deltaTime;
        if (usingWeapon && timer <= 0)
        {
            timer = cooldown;
            if (ammo <= 0)
                NoAmmo();
            else
                shoot();
            ammo--;
        }
    }

    private void shoot()
    {
        GameObject spawnedBullet = GameObject.Instantiate(bullet, transform.position, Quaternion.identity);
        spawnedBullet.layer = transform.parent.gameObject.layer;
        List<PlayerInput> players = GameObject.FindObjectsOfType<PlayerInput>().ToList();
        players.Remove(transform.parent.GetComponent<PlayerInput>());
        int random = Random.Range(0, players.Count - 1);
        spawnedBullet.GetComponent<FlipFlopBullet>().SetTarget(players[random].transform);
    }
}
