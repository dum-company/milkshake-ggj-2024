using System.Runtime.CompilerServices;
using Unity.VisualScripting;
using UnityEditor.ShortcutManagement;
using UnityEngine;

public class FlipFlopBullet : MonoBehaviour
{
    [SerializeField] private Rigidbody rb;
    [SerializeField] private float speed;
    [SerializeField, Range(0, 5)] private float desiredProximity;
    [SerializeField] private float shootHeight;
    [SerializeField, Range(0, 20)] private int damage;
    private Transform target;
    Vector3 facePosition = Vector3.zero;
    private bool goUp = true;
    private bool goDown = false;

    public void SetTarget(Transform value)
    {
        target = value;
    }

    void Update()
    {
        setTargetPosition();
        checkDistance();
        setRotation();
        move();
    }

    private void setTargetPosition()
    {
        facePosition = transform.position;
        facePosition.y = shootHeight;
        
        if (!goUp)
        {
            facePosition.x = target.position.x;
            facePosition.z = target.position.z;
        }
        if (goDown)
            facePosition.y = target.position.y;
    }

    private void checkDistance()
    {
        Vector3 horizontalPosition = transform.position;
        horizontalPosition.y = target.position.y;
        float distance = Vector3.Distance(target.position, horizontalPosition);
        if (transform.position.y >= shootHeight)
            goUp = false;
        if (distance <= desiredProximity)
            goDown = true;
    }

    private void setRotation()
    {
        transform.LookAt(facePosition, Vector3.up);
    }

    private void move()
    {
        rb.velocity = transform.forward * speed;
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.transform.tag == "Player")
            col.gameObject.GetComponent<PlayerHealth>().Damage(damage);
        Destroy(this.gameObject);
    }
}
