using UnityEngine;

public class ChickenWeapon : MonoBehaviour, IPlayerWeapon
{
    #region  interface
    public GameObject Object 
    {
        get => this.gameObject;
    }

    private PlayerAttack playerScript;
    public PlayerAttack PlayerScript 
    {
        get => playerScript;
        set => playerScript = value;
    }

    public void UseWeapon(bool performed)
    {
        usingWeapon = performed;
    }

    public void NoAmmo()
    {
        PlayerScript.SelectWeapon(0, usingWeapon);
        usingWeapon = false;
        timer = 0;
        SetMaxAmmo();
    }

    public void SetMaxAmmo()
    {
        ammo = maxAmmo;
    }
    #endregion

    [SerializeField] private SoundPlayer sP;
    [SerializeField] private GameObject bullet;
    [SerializeField, Range(1, 5)] private int maxAmmo = 3;
    [SerializeField, Range(0, 5)] private float cooldown;
    [SerializeField] GameObject chickenPrefab;
    private int ammo;
    float timer = 0;
    private bool usingWeapon = false;

    void Update()
    {
        if (timer > 0)
            timer -= Time.deltaTime;
        if (usingWeapon && timer <= 0)
        {
            timer = cooldown;
            ammo--;
            if (ammo <= 0)
                NoAmmo();
            else
                shoot();
        }
    }

    private void shoot()
    {
        sP.PlaySound("Shoot");
        GameObject spawnedBullet = GameObject.Instantiate(bullet, transform.position, Quaternion.identity);
        Instantiate(chickenPrefab, transform.position, Quaternion.identity);
        spawnedBullet.layer = transform.parent.gameObject.layer;
        spawnedBullet.GetComponent<EggBullet>().SetTarget(transform.forward);
    }
}
