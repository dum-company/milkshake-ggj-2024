using UnityEngine;

public class EggBullet : MonoBehaviour
{
    [SerializeField] private Rigidbody rb;
    [SerializeField] private SoundPlayer sP;
    [SerializeField] private float speed;
    [SerializeField] private float duration;
    [SerializeField, Range(0, 5)] private float explosionRadius;
    [SerializeField, Range(0, 20)] private int damage;

    public void SetTarget(Vector3 value)
    {
        Vector3 target = transform.position;
        target.x = transform.position.x + value.x;
        target.z = transform.position.z + value.z;
        target.y += 0.3f;
        transform.LookAt(target, Vector3.up);
        rb.velocity = transform.forward * speed;
    }

    void Update()
    {
        duration -= Time.deltaTime;
        if (duration <= 0)
            explode();
    }

    private void explode()
    {
        sP.PlaySound("Eggsplosion");
        Destroy(this.gameObject, 0.2f);
    }

    void OnCollisionEnter(Collision col)
    {
        sP.PlaySound("Hit");
        explode();
    }

    void OnDestroy()
    {
        Collider[] cols = Physics.OverlapSphere(transform.position, explosionRadius);
        foreach (Collider col in cols)
            if (col.transform.tag == "Player") 
            col.gameObject.GetComponent<PlayerHealth>().Damage(damage);
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
}
