using UnityEngine;

public class ChargeWeapon : MonoBehaviour, IPlayerWeapon
{
    #region interface
    public GameObject Object 
    {
        get => this.gameObject;
    }

    private PlayerAttack playerScript;
    public PlayerAttack PlayerScript 
    {
        get => playerScript;
        set => playerScript = value;
    }

    public void UseWeapon(bool performed)
    {
        if (performed)
            startCharge();
        else
        {
            if (usingWeapon)
                stopCharge();
            usingWeapon = false;
        }
    }

    public void NoAmmo()
    {
        PlayerScript.SelectWeapon(0, usingWeapon);
        usingWeapon = false;
        chargeTimer = float.NaN;
        SetMaxAmmo();
    }

    public void SetMaxAmmo()
    {
        ammo = maxAmmo;
    }
    #endregion

    [SerializeField] private SoundPlayer sP;
    [SerializeField] private PlayerMovementScript playerMovement;
    [SerializeField] private float chargeDuration;
    [SerializeField, Range(1, 5)] private int maxAmmo = 1;
    private int ammo;
    private float chargeTimer;
    private bool usingWeapon = false;
    private bool charging = false;

    void Update()
    {
        if (charging)
        {
            chargeTimer -= Time.deltaTime;
            if (chargeTimer <= 0)
                stopCharge();
        }
    }

    private void startCharge()
    {
        usingWeapon = true;
        charging = true;
        playerMovement.charging = true;
        chargeTimer = chargeDuration;
        sP.PlaySound("Charge");
        sP.PlaySound("Fart");
    }

    private void stopCharge()
    {
        charging = false;
        playerMovement.charging = false;
        chargeTimer = 0;
        NoAmmo();
        sP.StopSound("Charge");
    }
}