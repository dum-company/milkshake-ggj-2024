using UnityEngine;

public interface IPlayerWeapon
{
    public GameObject Object
    {
        get;
    }

    public PlayerAttack PlayerScript
    {
        get;
        set;
    }

    public void UseWeapon(bool performed);
    public void NoAmmo();
    public void SetMaxAmmo();
}
