using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TrapSpawner : MonoBehaviour
{
    [SerializeField] private GameObject traktorPrefab;
    [SerializeField] private float traktorSpawnHeight = 50f;
    [SerializeField] private GameObject knifePrefab;
    [SerializeField] private float knifeSpawnHeight = 60f;
    [SerializeField] private GameObject swarmPrefab;

    [Header("Warn")]
    [SerializeField] private float warningTime;
    [SerializeField] private float spawnTime;
    [SerializeField] private GameObject alarm;
    [SerializeField] private Image signImage;
    [SerializeField] private Sprite[] signs;

    void Start()
    {
        PowerUpSpawner.TrapEvent += spawnTrap;
    }

    private void spawnTrap(int id)
    {
        StartCoroutine(warn(id));
    }

    private IEnumerator warn(int id)
    {
        yield return new WaitForSeconds(warningTime);
        alarm.gameObject.SetActive(true);
        signImage.sprite = signs[id];
        StartCoroutine(spawnObject(id ));

    }

    IEnumerator spawnObject(int id)
    {
        yield return new WaitForSeconds(spawnTime);
        if (id == 0)
            spawnTraktor();
        else if (id == 1)
            spawnKnifes();
        else
            spawnSwarm();
        alarm.gameObject.SetActive(false);
    }

    void spawnTraktor()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("Player");
        Transform t = objs[Random.Range(0, objs.Length)].transform;
        GameObject.Instantiate(traktorPrefab, new Vector3(t.position.x, traktorSpawnHeight, t.position.z), Quaternion.identity);

    }

    void spawnKnifes()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("Player");
        Transform t = objs[Random.Range(0, objs.Length)].transform;
        GameObject.Instantiate(knifePrefab, new Vector3(t.position.x, knifeSpawnHeight, t.position.z), Quaternion.identity);
    }

    void spawnSwarm()
    {
        GameObject.Instantiate(swarmPrefab);
    }

    void OnDestroy()
    {
        PowerUpSpawner.TrapEvent -= spawnTrap;
    }
}
