using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarbecueDamage : MonoBehaviour
{
    [SerializeField] ParticleSystem[] smoke;
    [SerializeField] GameObject animeLines;

    void Update(){
        if (TimerControl.timeOut){
            transform.localScale += new Vector3(3f * Time.deltaTime, 0, 3f * Time.deltaTime);
            animeLines.SetActive(true);
        }
    }

   

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player"){
            collision.gameObject.GetComponent<PlayerHealth>().Damage(10);
            foreach (ParticleSystem s in smoke)
            {   
                s.transform.position = collision.gameObject.transform.position;
                s.Play();
            }
        }
    }
}
