using System.Collections;
using UnityEngine;

public class SwarmSpawner : MonoBehaviour
{
    [SerializeField] GameObject swarmPrefab;
    [SerializeField] GameObject swarmAlarm;


    [SerializeField] float warningTime;
    [SerializeField] float spawnTime;

    void Start()
    {
        StartCoroutine(warnSwarm());
    }

    IEnumerator warnSwarm()
    {
        yield return new WaitForSeconds(warningTime);
        swarmAlarm.gameObject.SetActive(true);
        
        StartCoroutine(spawnSwarm());

    }

    IEnumerator spawnSwarm()
    {
        yield return new WaitForSeconds(spawnTime);
        Instantiate(swarmPrefab, transform.position, Quaternion.identity,transform);
        swarmAlarm.gameObject.SetActive(false);


    }



}
