using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class SwarmMove : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] float speed;
    [SerializeField] int damage;


    private void Start()
    {
    }
    // Update is called once per frame
    void Update()
    {
        transform.Translate(transform.right * speed * Time.deltaTime);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
            other.gameObject.GetComponent<PlayerHealth>().Damage(damage);
        if (other.gameObject.tag == "FleaEnd")
            Destroy(gameObject);
        
    }
}
