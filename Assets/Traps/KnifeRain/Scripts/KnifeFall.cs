using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class KnifeFall : MonoBehaviour
{
    [SerializeField] int damage;
    [SerializeField] Rigidbody rigidBody;
    [SerializeField] float minimalForce;
    [SerializeField] float maximumForce;

    void Start(){
        rigidBody.AddForce(-transform.up * Random.Range(minimalForce,maximumForce));
    }
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag != "Knife")
        {
            if (other.gameObject.tag == "Player")
                other.gameObject.GetComponent<PlayerHealth>().Damage(damage);
            Destroy(this.transform.parent.gameObject);
        }
    }
}
