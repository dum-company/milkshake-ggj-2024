using UnityEngine;

public class TimedDestroy : MonoBehaviour
{
    void Start()
    {
        Destroy(this.gameObject, 1f);
    }
}
